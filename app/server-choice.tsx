import React, { useCallback, useEffect, useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  FlatList,
} from "react-native";
import { ThemedText } from "@/components/ThemedText";
import { ThemedView } from "@/components/ThemedView";
import { debounce } from "lodash";
import { initializeApi, getApiInstance } from "@/api/axios"; // Use existing API functions
import Constants from "expo-constants";
import * as Linking from "expo-linking";
import { router } from "expo-router";
import { useMastodonAuth } from "@/context/MastodonAuthContext";
import AsyncStorage from "@react-native-async-storage/async-storage";

const BASE_URL = "https://instances.social/api";

interface Instance {
  id: string;
  name: string;
  active_users: number | null;
  open_registrations: boolean;
}

interface InstancesResponse {
  instances: Instance[];
}

const ServerChoice = () => {
  const [myServerName, setMyServerName] = useState("");
  const [servers, setServers] = useState<Instance[]>([]);
  const [isLoading, setIsLoading] = useState(false);
  const { accessToken, setAccessToken, setServerName } = useMastodonAuth();

  const fetchServers = useCallback(
    debounce(async (query: string) => {
      if (query.length < 3) {
        setServers([]);
        return;
      }

      setIsLoading(true);
      initializeApi(BASE_URL, Constants.expoConfig?.extra?.INSTANCES_TOKEN);
      const axiosInstance = getApiInstance();

      try {
        const response = await axiosInstance.get<InstancesResponse>(
          "/1.0/instances/search",
          { params: { q: query, count: 10, name: true } }
        );
        setServers(response.data.instances);
      } catch (error) {
        console.error("Failed to fetch servers:", error);
      } finally {
        setIsLoading(false);
      }
    }, 300),
    []
  );

  useEffect(() => {
    fetchServers(myServerName);
  }, [myServerName, fetchServers]);

  useEffect(() => {
    const handleDeepLink = async (event: { url: string }) => {
      const url = Linking.parse(event.url);
      const code = url.queryParams?.code;

      if (code && myServerName) {
        try {
          initializeApi(`https://${myServerName}`);
          const axiosInstance = getApiInstance();

          const response = await axiosInstance.post("/oauth/token", {
            client_id: Constants.expoConfig?.extra?.CLIENT_ID,
            client_secret: Constants.expoConfig?.extra?.CLIENT_SECRET,
            redirect_uri: "pugdom://oauth",
            grant_type: "authorization_code",
            code,
          });

          const accessToken = response.data.access_token;
          setAccessToken(accessToken);
          setServerName(myServerName);
          // await AsyncStorage.setItem("mastodonServer", serverName);
          router.push("/(tabs)");
        } catch (error) {
          console.error("Failed to exchange code for access token:", error);
        }
      }
    };

    const subscription = Linking.addEventListener("url", handleDeepLink);
    Linking.getInitialURL().then((initialUrl) => {
      if (initialUrl) {
        handleDeepLink({ url: initialUrl });
      }
    });

    return () => subscription.remove();
  }, [myServerName]);

  const handleServerLogin = async (name: string) => {
    try {
      setMyServerName(name);
      const clientId = Constants.expoConfig?.extra?.CLIENT_ID;
      if (!clientId) throw new Error("Client ID is missing in configuration.");

      const queryParams = new URLSearchParams({
        response_type: "code",
        client_id: clientId,
        redirect_uri: "pugdom://oauth",
      });

      const url = `https://${name}/oauth/authorize?${queryParams.toString()}`;
      await Linking.openURL(url);
    } catch (error) {
      console.error("Error during server login:", error);
    }
  };

  return (
    <ThemedView style={styles.container}>
      <ThemedText>What server are you in to?</ThemedText>
      <TextInput
        style={styles.textInputStyle}
        placeholder={"What's Your Server Name?"}
        value={myServerName}
        onChangeText={setMyServerName}
        autoCapitalize="none"
      />

      {myServerName.length >= 3 && (
        <View style={styles.dropdown}>
          {isLoading ? (
            <Text>Loading...</Text>
          ) : servers.length > 0 ? (
            <FlatList
              data={servers}
              renderItem={({ item }) => (
                <TouchableOpacity
                  style={styles.dropdownItem}
                  onPress={() => handleServerLogin(item.name)}
                >
                  <Text>{item.name}</Text>
                </TouchableOpacity>
              )}
              keyExtractor={(item) => item.id}
              style={styles.dropdownList}
            />
          ) : (
            <Text>No servers found</Text>
          )}
        </View>
      )}
    </ThemedView>
  );
};

export default ServerChoice;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  textInputStyle: {
    height: 40,
    borderColor: "gray",
    borderWidth: 1,
    paddingHorizontal: 10,
    borderRadius: 5,
    marginTop: 5,
    width: "50%",
  },
  dropdown: {
    width: "50%",
    maxHeight: 150,
    marginTop: 5,
    borderColor: "gray",
    borderWidth: 1,
    borderRadius: 5,
    backgroundColor: "white",
  },
  dropdownList: {
    flexGrow: 0,
  },
  dropdownItem: {
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: "#eee",
  },
});
