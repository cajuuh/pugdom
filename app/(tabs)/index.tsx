import { Image, StyleSheet, Platform } from "react-native";

import { HelloWave } from "@/components/HelloWave";
import ParallaxScrollView from "@/components/ParallaxScrollView";
import { ThemedText } from "@/components/ThemedText";
import { ThemedView } from "@/components/ThemedView";
import { useMastodonAuth } from "@/context/MastodonAuthContext";

export default function HomeScreen() {
  const { accessToken, serverName } = useMastodonAuth();
  return (
    <ThemedView style={styles.container}>
      <ThemedText>Home Screen</ThemedText>
      <ThemedText>accessToken: {accessToken}</ThemedText>
      <ThemedText>serverName: {serverName}</ThemedText>
    </ThemedView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});
