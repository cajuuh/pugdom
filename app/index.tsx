import React from "react";
import { ActivityIndicator, View } from "react-native";
import { Redirect } from "expo-router";
import { useMastodonAuth } from "@/context/MastodonAuthContext";

export default function Index() {
  const { accessToken } = useMastodonAuth();

  if (accessToken === undefined) {
    return (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <ActivityIndicator size="large" />
      </View>
    );
  }

  return <Redirect href={accessToken ? "/(tabs)" : "/server-choice"} />;
}
