import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse } from "axios";

let axiosInstance: AxiosInstance | null = null;

export const initializeApi = (baseURL: string, bToken?: string) => {
  console.log(`Initializing API with baseURL: ${baseURL}`);

  axiosInstance = axios.create({
    baseURL,
    timeout: 10000,
    headers: {
      "Content-Type": "application/json",
    },
  });

  axiosInstance.interceptors.request.use(
    (config) => {
      if (bToken) {
        config.headers = config.headers || {};
        config.headers.Authorization = `Bearer ${bToken}`;
      }
      return config;
    },
    (error) => Promise.reject(error)
  );

  axiosInstance.interceptors.response.use(
    (response) => response,
    (error) => Promise.reject(error)
  );
};

export const getApiInstance = (): AxiosInstance => {
  if (!axiosInstance) {
    throw new Error("API instance not initialized. Call initializeApi first.");
  }
  return axiosInstance;
};

export const getData = async <T>(
  url: string,
  config?: AxiosRequestConfig
): Promise<T> => {
  const instance = getApiInstance();
  try {
    const response: AxiosResponse<T> = await instance.get(url, config);
    return response.data;
  } catch (error) {
    console.error("GET request failed:", error);
    throw error;
  }
};

export const postData = async <T>(
  url: string,
  data?: any,
  config?: AxiosRequestConfig
): Promise<T> => {
  const instance = getApiInstance();
  try {
    const response: AxiosResponse<T> = await instance.post(url, data, config);
    return response.data;
  } catch (error) {
    console.error("POST request failed:", error);
    throw error;
  }
};
