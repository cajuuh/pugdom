// Define the shape of a post
export interface Post {
  id: number;
  title: string;
  body: string;
  userId: number;
}

// Define the shape of the response for creating a post
export interface CreatePostResponse {
  id: number; // The ID of the newly created post
}
