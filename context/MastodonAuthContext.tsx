import React, { createContext, useContext, useState, useEffect } from "react";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { initializeApi } from "@/api/axios"; // Import your API setup

const MastodonAuthContext = createContext<{
  accessToken: string | null | undefined;
  setAccessToken: (token: string | null) => void;
  serverName: string | null | undefined;
  setServerName: (server: string | null) => void;
}>({
  accessToken: null,
  setAccessToken: () => {},
  serverName: null,
  setServerName: () => {},
});

export const MastodonAuthProvider = ({
  children,
}: {
  children: React.ReactNode;
}) => {
  const [accessToken, setAccessToken] = useState<string | null | undefined>(
    undefined
  );
  const [serverName, setServerName] = useState<string | null | undefined>(
    undefined
  );

  useEffect(() => {
    const loadAuthData = async () => {
      try {
        const storedToken = await AsyncStorage.getItem("mastodonAccessToken");
        const storedServer = await AsyncStorage.getItem("mastodonServerName");

        setAccessToken(storedToken ?? null);
        setServerName(storedServer ?? null);

        if (storedServer) {
          initializeApi(`https://${storedServer}`, storedToken ?? undefined);
        }
      } catch (error) {
        console.error("Error loading auth data:", error);
      }
    };
    loadAuthData();
  }, []);

  useEffect(() => {
    const saveAuthData = async () => {
      if (accessToken !== undefined) {
        if (accessToken) {
          await AsyncStorage.setItem("mastodonAccessToken", accessToken);
        } else {
          console.warn("Removing Token");
          await AsyncStorage.removeItem("mastodonAccessToken");
        }
      }

      if (serverName !== undefined) {
        if (serverName) {
          await AsyncStorage.setItem("mastodonServerName", serverName);
        } else {
          console.warn("Removing Server Name");
          await AsyncStorage.removeItem("mastodonServerName");
        }
      }
    };
    saveAuthData();
  }, [accessToken, serverName]);

  useEffect(() => {
    if (serverName) {
      initializeApi(`https://${serverName}`, accessToken ?? undefined);
    }
  }, [serverName, accessToken]);

  return (
    <MastodonAuthContext.Provider
      value={{ accessToken, setAccessToken, serverName, setServerName }}
    >
      {children}
    </MastodonAuthContext.Provider>
  );
};

export const useMastodonAuth = () => useContext(MastodonAuthContext);
