import * as SecureStore from "expo-secure-store";

// Save the access token
export const saveAccessToken = async (token: string) => {
  await SecureStore.setItemAsync("mastodonAccessToken", token);
};

// Retrieve the access token
export const getAccessToken = async () => {
  return await SecureStore.getItemAsync("mastodonAccessToken");
};

// Remove the access token
export const removeAccessToken = async () => {
  await SecureStore.deleteItemAsync("mastodonAccessToken");
};
